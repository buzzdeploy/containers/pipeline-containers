[[_TOC_]]

# Pipeline Containers 

This container builds the pipeline optimized containers for building the infrastructure as code containers.

Reason for use: Smaller and focused containers allow for faster pipelines with better control of the dependencies.

# Contents 

These contents should show up in the container registry for use.

## Terraform
* Controlled Terraform container for tracking purposes
* All providers needed for the pipeline are included for streamlined use
* All provider modules are tracked for the sake of transparency

**Use:** Any Terraform deployments needed in pipeline

## Linode
* Ubuntu base container built from scratch
* Python installed with setup tools 
* Linode CLI installed 
* Ansible

**Use:** Any Ansible or Linode CLI specific operations
