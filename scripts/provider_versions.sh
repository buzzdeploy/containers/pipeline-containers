#!/bin/bash

case $1 in 

	"1.0.0")
		TEMPLATE_PROVIDER_VERSION="2.2.0"
		HTTP_PROVIDER_VERSION="3.1.0"
        ARCHIVE_PROVIDER_VERSION="2.1.0"
        RANDOM_PROVIDER_VERSION="2.3.0"
        LOCAL_PROVIDER_VERSION="1.3.0"
        LINODE_PROVIDER_VERSION="1.29.2"

	;;

	*)
		echo -n "Unknown provider version selection"
	;;
esac