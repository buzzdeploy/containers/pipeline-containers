#!/bin/bash
set -euxo pipefail

# These should be input variables to support multiple versions
source /runner/provider_versions.sh $1
HASHICORP_PROVIDER_URL="https://releases.hashicorp.com"

# http
wget -q -nc ${HASHICORP_PROVIDER_URL}/terraform-provider-http/${HTTP_PROVIDER_VERSION}/terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip 
mkdir -p ./registry.terraform.io/hashicorp/http/${HTTP_PROVIDER_VERSION}/linux_amd64/
unzip -o terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/http/${HTTP_PROVIDER_VERSION}/linux_amd64/"
rm terraform-provider-http_${HTTP_PROVIDER_VERSION}_linux_amd64.zip

# random
wget -q -nc ${HASHICORP_PROVIDER_URL}/terraform-provider-random/${RANDOM_PROVIDER_VERSION}/terraform-provider-random_${RANDOM_PROVIDER_VERSION}_linux_amd64.zip 
mkdir -p ./registry.terraform.io/hashicorp/random/${RANDOM_PROVIDER_VERSION}/linux_amd64/
unzip -o terraform-provider-random_${RANDOM_PROVIDER_VERSION}_linux_amd64.zip  -d "/plugins/registry.terraform.io/hashicorp/random/${RANDOM_PROVIDER_VERSION}/linux_amd64/"
rm terraform-provider-random_${RANDOM_PROVIDER_VERSION}_linux_amd64.zip

# template
wget -q -nc ${HASHICORP_PROVIDER_URL}/terraform-provider-template/${TEMPLATE_PROVIDER_VERSION}/terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip 
mkdir -p ./registry.terraform.io/hashicorp/template/${TEMPLATE_PROVIDER_VERSION}/linux_amd64/
unzip -o terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/template/${TEMPLATE_PROVIDER_VERSION}/linux_amd64/"
rm terraform-provider-template_${TEMPLATE_PROVIDER_VERSION}_linux_amd64.zip

# Local
wget -q -nc ${HASHICORP_PROVIDER_URL}/terraform-provider-local/${LOCAL_PROVIDER_VERSION}/terraform-provider-local_${LOCAL_PROVIDER_VERSION}_linux_amd64.zip 
mkdir -p ./registry.terraform.io/hashicorp/local/${LOCAL_PROVIDER_VERSION}/linux_amd64/
unzip -o terraform-provider-local_${LOCAL_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/local/${LOCAL_PROVIDER_VERSION}/linux_amd64/"
rm terraform-provider-local_${LOCAL_PROVIDER_VERSION}_linux_amd64.zip

# linode
wget ${HASHICORP_PROVIDER_URL}/terraform-provider-linode/${LINODE_PROVIDER_VERSION}/terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip
mkdir -p /plugins/registry.terraform.io/hashicorp/linode/${LINODE_PROVIDER_VERSION}/linux_amd64/
unzip -o terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip -d "/plugins/registry.terraform.io/hashicorp/linode/${LINODE_PROVIDER_VERSION}/linux_amd64/"
rm terraform-provider-linode_${LINODE_PROVIDER_VERSION}_linux_amd64.zip
