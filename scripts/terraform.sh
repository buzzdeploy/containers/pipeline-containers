#!/bin/bash

set -euxo pipefail

TF_VERSION="$1"
TFSEC_VERSION="$2"
# Terraform 

wget -q -nc "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip"
unzip terraform_${TF_VERSION}_linux_amd64.zip -d /usr/local/bin
rm terraform_${TF_VERSION}_linux_amd64.zip


wget -q -nc https://github.com/aquasecurity/tfsec/releases/download/${TFSEC_VERSION}/tfsec-linux-amd64 -O tfsec
chmod +x tfsec
mv tfsec /usr/local/bin/
