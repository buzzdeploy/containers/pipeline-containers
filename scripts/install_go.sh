#!/bin/bash
set -euxo pipefail

GO_VERSION="$1"
curl -OL https://golang.org/dl/go${GO_VERSION}.linux-amd64.tar.gz
tar -C /usr/local -xvf go${GO_VERSION}.linux-amd64.tar.gz
cp /usr/local/go/bin/go* /usr/bin/.

# apt-get install -y -qq go

mkdir -p ~/go/src
export GOPATH=~/go
